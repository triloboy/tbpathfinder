﻿using System;
using System.Collections.Generic;

namespace TbPathFinder
{
    public class PathNotFoundException : PathException
    { }

    public class PathFinder : IPathFinder
    {
        private ITileMap _tileMap;
        private bool _loose;

        public ITileMap TileMap
        {
            get { return _tileMap; }
        }

        public PathFinder(ITileMap tileMap, bool loose = false)
        {
            _tileMap = tileMap;
            _loose = loose;
        }

        private IEnumerable<Point> Reconstruct(IDictionary<Point, Point> cameFrom, Point current)
        {
            var path = new Stack<Point>();

            // skip the last one
            if (_loose == true)
            {
                current = cameFrom[current];
            }

            while (cameFrom.ContainsKey(current))
            {
                path.Push(current);
                current = cameFrom[current];
            }

            return path;
        }

        private static Point FindPointWithLowCost(Dictionary<Point, int> potentialCosts)
        {
            int bestCost = int.MaxValue;
            Point bestPoint = default(Point);

            foreach (var pair in potentialCosts)
            {
                int currentCost = pair.Value;
                if (currentCost < bestCost)
                {
                    bestCost = currentCost;
                    bestPoint = pair.Key;
                }
            }

            return bestPoint;
        }

        public IEnumerable<Point> FindPath(Point start, Point destination)
        {
            List<Point> neighbors = new List<Point>();

            //TODO: SortedSet was removed for compatibility reasons
            var open = new HashSet<Point>();
            var potentialCosts = new Dictionary<Point, int>();
            
            var closed = new HashSet<Point>();
            var cameFrom = new Dictionary<Point, Point>();

            var costs = new Dictionary<Point, int>();
            costs[start] = 0;

            //costWithHeuristic[start] = PathFinderHelper.ManhattanDistance(start, destination);
            int cost = PathFinderHelper.ChebyshevDistance(start, destination);
            open.Add(start);
            potentialCosts.Add(start, cost);

            while (open.Count > 0)
            {
                //TODO: use something more suitable to get rid of O(n) complexity
                Point current = FindPointWithLowCost(potentialCosts);
                open.Remove(current);
                potentialCosts.Remove(current);

                if (current == destination)
                {
                    return Reconstruct(cameFrom, current);
                }

                closed.Add(current);
                Expand(current, destination, neighbors);
                foreach (var neighbor in neighbors)
                {
                    if (closed.Contains(neighbor))
                        continue;

                    cost = costs[current] + 1;

                    if (!costs.ContainsKey(neighbor) || cost < costs[neighbor])
                    {
                        cameFrom[neighbor] = current;
                        costs[neighbor] = cost;

                        // let's calculate potential cost
                        cost = cost + PathFinderHelper.ChebyshevDistance(neighbor, destination);
                        if (!open.Contains(neighbor))
                        {
                            open.Add(neighbor);
                        }
                        potentialCosts[neighbor] = cost;
                    }
                }
            }
            throw new PathNotFoundException();
        }

        private void Expand(Point source, Point destination, List<Point> result)
        {
            result.Clear();
            foreach (var direction in PathFinderHelper.Directions)
            {
                Point point = source + direction;
                if (_tileMap.IsPassable(point))
                {
                    result.Add(point);
                    continue;
                }
                if (_loose)
                {
                    if (point == destination)
                    {
                        result.Add(point);
                    }
                }
            }
        }
    }
}
