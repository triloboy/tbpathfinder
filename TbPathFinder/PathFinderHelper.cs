﻿using System;
using System.Collections.Generic;

namespace TbPathFinder
{
    internal static class PathFinderHelper
    {
        internal static readonly Point[] Directions = {
			new Point(1, 0), new Point(-1, 0),
			new Point(0, 1), new Point(0, -1),
			new Point(1, 1), new Point(-1, -1),
			new Point(-1, 1), new Point(1, -1)
		};

        internal static int ChebyshevDistance(Point source, Point destination)
        {
            return Math.Max(Math.Abs(destination.X - source.X), Math.Abs(destination.Y - source.Y));
        }
    }
}
