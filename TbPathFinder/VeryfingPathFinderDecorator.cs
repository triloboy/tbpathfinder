﻿using System;
using System.Collections.Generic;

namespace TbPathFinder
{
    public class PathBlockedException : PathException
    {

    }

    public class VeryfingPathFinderDecorator : PathFinderDecorator
    {
        public VeryfingPathFinderDecorator(IPathFinder pathFinder) : base(pathFinder)
        { }

        public override IEnumerable<Point> FindPath(Point start, Point destination)
        {
            while (true)
            {
                var path = _decoratedPathFinder.FindPath(start, destination);
                IEnumerator<Point> enumerator = path.GetEnumerator();

                // we are not able to move next, so we are at destination
                if (enumerator.MoveNext() == false)
                {
                    yield break;
                }

                // the first one is obviously passable, we don't need to check again
                yield return enumerator.Current;

                var tileMap = TileMap;
                while (enumerator.MoveNext())
                {
                    Point current = enumerator.Current;
                    if (tileMap.IsPassable(current) == false)
                    {
                        throw new PathBlockedException();
                    }
                }
            }
        }
    }
}
