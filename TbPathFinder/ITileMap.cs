﻿using System;
using System.Collections.Generic;

namespace TbPathFinder
{
    public interface ITileMap
    {
        bool IsPassable(Point point);
    }
}
