﻿using System;
using System.Collections.Generic;

namespace TbPathFinder
{
    public abstract class PathFinderDecorator : IPathFinder
    {
        protected IPathFinder _decoratedPathFinder;

        public PathFinderDecorator(IPathFinder decoratedPathFinder)
        {
            _decoratedPathFinder = decoratedPathFinder;
        }

        public ITileMap TileMap
        {
            get { return _decoratedPathFinder.TileMap; }
        }

        public abstract IEnumerable<Point> FindPath(Point start, Point destination);
    }
}
