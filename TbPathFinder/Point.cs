﻿using System;
using System.Collections.Generic;

namespace TbPathFinder
{
    public struct Point
    {
        public short X;
        public short Y;

        public Point(float x, float y)
        {
            X = (short)x;
            Y = (short)y;
        }

        public Point(short x, short y)
        {
            X = x;
            Y = y;
        }

        public static Point operator +(Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        public static Point operator -(Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }

        public static bool operator ==(Point a, Point b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Point a, Point b)
        {
            return a.X != b.X || a.Y != b.Y;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Point))
            {
                return false;
            }
            Point point = (Point)obj;
            return this == point;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int)X << 16) | (ushort)Y;
            }
        }

        public override string ToString()
        {
            return string.Format("Point({0}, {1})", X, Y);
        }
    }
}
