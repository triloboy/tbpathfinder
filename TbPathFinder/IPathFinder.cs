﻿using System;
using System.Collections.Generic;

namespace TbPathFinder
{
    public interface IPathFinder
    {
        ITileMap TileMap { get; }
        IEnumerable<Point> FindPath(Point start, Point destination);
    }
}
