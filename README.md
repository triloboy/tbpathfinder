TbPathFinder
============

Build status: [![Build status](https://ci.appveyor.com/api/projects/status/h3cgun6qaqf70toa/branch/master?svg=true)](https://ci.appveyor.com/project/triloboy/tbpathfinder/branch/master)  
The latest stable: https://ci.appveyor.com/project/triloboy/tbpathfinder/build/artifacts  

Description
-----------

Path finder using A* algorithm usable in Unity game engine for uniform grid maps.

Usage
-----

First create TileMap class:
```
#!CSharp
// class that represents tile map
public class TileMap : ITileMap
{
    public bool IsPassable(Point point)
    {
        Vector2 vector = new Vector2(point.X + 0.5f, point.Y + 0.5f);
        var collider = Physics2D.OverlapPoint(vector);
        return collider == null;
    }
}
```

Then use PathFinder:
```
#!CSharp
var map = new TileMap();
// true -> destination is excluded for this path finder, useful for finding path to a container (chest) for example
var pathFinder = new PathFinder(map, true);
var points = pathFinder.FindPath(new Point(0, 0), new Point(2, 0));

```

License
-------

Mozilla Public License version 2.

Fore more information please read LICENSE.txt.
