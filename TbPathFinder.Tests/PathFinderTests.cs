﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace TbPathFinder.Tests
{
    class Map : Dictionary<Point, bool>, ITileMap
    {
        public bool IsPassable(Point point)
        {
            if (point.X < 0 || point.Y < 0)
                return false;
            bool value = true;
            if (TryGetValue(point, out value))
                return value;
            return true;
        }
    }

    [TestFixture]
    internal class PathFinderTests
    {
        [Test]
        public void TestAvoidingObstacles()
        {
            var map = new Map();
            map[new Point(1, 0)] = false;
            map[new Point(1, 1)] = false;

            var pathFinder = new PathFinder(map, false);

            var pathEnumerable = pathFinder.FindPath(new Point(0, 0), new Point(2, 0));
            var path = pathEnumerable.ToList();

            Assert.AreEqual(path.Count(), 4);
            Assert.AreEqual(path[0], new Point(0, 1));
            Assert.AreEqual(path[1], new Point(1, 2));
            Assert.AreEqual(path[2], new Point(2, 1));
            Assert.AreEqual(path[3], new Point(2, 0));
        }

        [Test]
        public void TestPathNotFound()
        {
            var map = new Map();
            map[new Point(1, 0)] = false;
            map[new Point(1, 1)] = false;
            map[new Point(0, 1)] = false;

            var pathFinder = new PathFinder(map, false);
            Assert.Throws<PathNotFoundException>(() => pathFinder.FindPath(new Point(0, 0), new Point(2, 0)));
        }

        [Test]
        public void TestPathNotFoundExcludedDestination()
        {
            var map = new Map();
            map[new Point(1, 0)] = false;
            map[new Point(1, 1)] = false;
            map[new Point(0, 1)] = false;

            var pathFinder = new PathFinder(map, true);
            Assert.Throws<PathNotFoundException>(() => pathFinder.FindPath(new Point(0, 0), new Point(2, 0)));
        }

        [Test]
        public void TestAvoidingObstaclesLoose()
        {
            var map = new Map();
            map[new Point(1, 0)] = false;
            map[new Point(1, 1)] = false;

            var pathFinder = new PathFinder(map, true);

            var pathEnumerable = pathFinder.FindPath(new Point(0, 0), new Point(2, 0));
            var path = pathEnumerable.ToList();

            Assert.AreEqual(path.Count, 3);
            Assert.AreEqual(path[0], new Point(0, 1));
            Assert.AreEqual(path[1], new Point(1, 2));
            Assert.AreEqual(path[2], new Point(2, 1));
        }

        [Test]
        public void TryFind_ShouldFind_IfDestinationInaccessbileAndExcluded()
        {
            var map = new Map();
            map[new Point(1, 0)] = false;
            map[new Point(1, 1)] = false;
            map[new Point(2, 0)] = false;

            var pathFinder = new PathFinder(map, true);

            var pathEnumerable = pathFinder.FindPath(new Point(0, 0), new Point(2, 0));
            var path = pathEnumerable.ToList();

            Assert.AreEqual(path.Count, 3);
            Assert.AreEqual(path[0], new Point(0, 1));
            Assert.AreEqual(path[1], new Point(1, 2));
            Assert.AreEqual(path[2], new Point(2, 1));
        }

        [Test]
        public void OneStepOnePoint()
        {
            var map = new Map();

            var pathFinder = new PathFinder(map, false);

            var pathEnumerable = pathFinder.FindPath(new Point(0, 0), new Point(1, 0));
            var path = pathEnumerable.ToList();

            Assert.AreEqual(path.Count, 1);
            Assert.AreEqual(path[0], new Point(1, 0));
        }
    }
}
